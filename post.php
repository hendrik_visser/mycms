<?php
include_once('includes/connection.php');
include_once('includes/post.php');

$post = new Post;

if (isset($_GET['id'])) {
	$id = $_GET['id'];
	$data = $post->fetch_data($id);
	?>
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Document</title>
		<link rel="stylesheet" href="assets/style.css">
	</head>
	<body>
		<div class="container">
			<a href="index.php" id="logo">CMS</a>
			<h4><?php echo $data['post_titel'];?>
				<small>
					Gepost - <?php echo date('l w F',$data['post_timestamp']); ?>
				</small>
			</h4>

			<p><?php echo $data['post_inhoud']; ?></p>
			<a href="index.php">&larr; Terug</a>
		</div>
	</body>
	</html>
	<?php

}else{
	header('Location: index.php');
	exit();
}



?>