<?php

session_start();

include_once('../includes/connection.php');
include_once('../includes/post.php');

$post = new Post;

if (isset($_SESSION['logged_in'])) {
	if (isset($_GET['id'])) {
		$id = $_GET['id'];

		$query = $pdo->prepare('DELETE FROM posts WHERE post_id = ?');
		$query->bindValue(1, $id);
		$query->execute();

		header('Location: delete.php');
	}

	$posts = $post->fetch_all();

	?>

	<!DOCTYPE html>
	<html lang="en">
		<head>
			<meta charset="UTF-8">
			<title>Document</title>
			<link rel="stylesheet" href="../assets/style.css">
		</head>
		<body>
			<div class="container">
				<a href="../index.php" id="logo">CMS</a>
				<br/>
				<h4>Selecteer een post om te verwijderen</h4>
				<form action="delete.php"  method="get">
					<select onchange="this.form.submit();" name="id">
						<?php foreach ($posts as $post ) { ?>
							<option value="<?php echo  $post['post_id']; ?>"><?php echo $post['post_titel']; ?>
							</option>
						<?php } ?>
					</select>
				</form>
				</div>
				<br/>
			</div>
		</body>
	</html>
	<?php

} else {
	header('Location: index.php');
}
?>