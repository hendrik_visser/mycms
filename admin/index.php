<?php 

session_start();

include_once('../includes/connection.php');

if (isset($_SESSION['logged_in'])){
	//display index
	?>
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Document</title>
		<link rel="stylesheet" href="../assets/style.css">
	</head>
	<body>
		<div class="container">
			<a href="../index.php" id="logo">CMS</a>
			<br/>
			<ol>
				<li><a href="add.php">Voeg post toe</a></li>
				<li><a href="delete.php">Verwijder een post</a></li>
				<li><a href="logout.php">Uitloggen</a></li>
			</ol>
			</div>
			<br/>

		</div>
	</body>
	</html>
	<?php
} else {
	if(isset($_POST['username'], $_POST['password'])){
		$username = $_POST['username'];
		$password = md5($_POST['password']);

		if (empty($username) or empty($password)){
			$error = 'Alle velden zijn verplicht!';
		} else {
			$query = $pdo->prepare("SELECT * FROM gebruikers WHERE gebruiker_naam = ? AND gebruiker_paswoord = ?");
			$query->bindValue(1, $username);
			$query->bindValue(2, $password);
			$query->execute();

			$num = $query->rowCount();

			if($num == 1){
				//gebruiker heeft de goede gegevens ingevoerd
				$_SESSION['logged_in'] = true;
				header('Location:index.php');
				echo 'je bent ingelogd';
				exit();
			} else {
				//gebruiker heeft foute gegevens ingevoerd
				$error = 'Gebruikersnaam of paswoord klopt niet!';
			}
		}
	}
	//display login
	?>
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Document</title>
		<link rel="stylesheet" href="../assets/style.css">
	</head>
	<body>
		<div class="container">
			<a href="index.php" id="logo">CMS</a>
			<br/><br/>
			<?php if (isset($error)) { ?>
				<small style="color:#aa0000;"><?php echo $error; ?></small>
		    <br/><br/>
			<?php } ?>

			<form action="index.php" method="post" autocomplete="off">
				<input type="text" name="username" placeholder="gebruikersnaam">
				<input type="password" name="password" placeholder="paswoord">
				<input type="submit" value="login">
			</form>
			</div>
			<br/>

		</div>
	</body>
	</html>
	<?php
}

 ?>