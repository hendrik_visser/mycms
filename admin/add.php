<?php 

session_start();

include_once('../includes/connection.php');

if (isset($_SESSION['logged_in'])) {
	# display add page

	# validation
	if (isset($_POST['title'], $_POST['content'])) {
		# assign variables
		$title = $_POST['title'];
		$content = nl2br($_POST['content']);
		# error message
		if (empty($title) or empty($content)) {
			$error = 'Alle velden moeten ingevuld worden!';
		} else {
			$query = $pdo->prepare('INSERT INTO posts (post_titel, post_inhoud, post_timestamp) VALUES (?,?,?)');
			$query->bindValue(1, $title);
			$query->bindValue(2, $content);
			$query->bindValue(3, time());

			$query->execute();

			header('Location:index.php');
		}
	}

	?>
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Document</title>
		<link rel="stylesheet" href="../assets/style.css">
	</head>
	<body>
		<div class="container">
			<a href="../index.php" id="logo">CMS</a>
			<br/>
			<h4>Post toevoegen</h4>

			<?php if (isset($error)) { ?>
				<small style="color:#aa0000;"><?php echo $error; ?></small>
		    <br/><br/>
			<?php } ?>

			<form action="add.php" method="post" autocomplete="off">
				<input type="text" name="title" placeholder="Titel"/> </br></br>
				<textarea name="content" cols="50" rows="15" placeholder="Tekst" ></textarea></br></br>
				<input type="submit" value="Voeg post toe">
			</form>
			</div>
			<br/>
			
		</div>
	</body>
	</html>
	<?php
} else {
	header('Location: index.php');
}

