<?php
include_once('includes/connection.php');
include_once('includes/post.php');



$post = new Post;
$posts = $post->fetch_all();

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="assets/style.css">
</head>
<body>
	<div class="container">
		<a href="index.php" id="logo">CMS</a>
		<ol>
			<?php foreach ($posts as $post) { ?>
			<li>
				<a href="post.php?id=<?php echo $post['post_id']; ?>">
				<?php echo $post['post_titel']; ?>
				</a>

				<small>
				
				Gepost - <?php echo date('l w F', $post['post_timestamp']); ?>
				</small>
			</li>
			<?php } ?>
		</ol>
		<small>
			<a href="admin">Admin</a>
		</small>
		</div>
		<br/>

	</div>
</body>
</html>